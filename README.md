This repository contains the source code of the data trading Android application as described in "Trading User Data: A Blockchain Based Approach".

A demo video of the application can be found [here](https://git.rwth-aachen.de/thorsten.weber/blockchain-trading-application/blob/master/Demo%20Video/Blockchain.mp4)